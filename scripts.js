"use strict";
/**
 * 1 - DOM - програмний інтерфейс для взаємодії з HTML. Має вигляд дерева, де всі елементи розгалуджуються від батьківського елементу. 
 * Тут кожен тег представлений у вигляді об'єкта, який ми можемо змінювати, видаляти, додавати... 
 * 
 * 2 - innerHTML виводить HTML вибраного елемента або всієї сторінки. innerText виводить текст всередині елементу.
 * 
 * 3 - Звернутися до елемента можна через getElementById(byClassName...), та за допомогою querySelector. 
 * Краще другий, так як він новіший і універсальний. Перший використовується переважно в старому коді.
 */

// Знайти всі параграфи на сторінці та встановити колір фону #ff0000

const allParagraphs = document.querySelectorAll('p');
for (let elem of allParagraphs) {
    elem.style.background = '#ff0000';
};
console.log(allParagraphs);

// ======================================================================

// Знайти елемент із id="optionsList". Вивести у консоль. 
// Знайти батьківський елемент та вивести в консоль. 
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

const optionsList = document.querySelector('#optionsList');
console.log(optionsList);
console.log(optionsList.parentNode);

optionsList.childNodes;

for (let elem of optionsList.childNodes) {
    console.log(`NodeName: ${elem.nodeName}, NodeType: ${elem.nodeType}`);
};

// ===================================================================
/**
 * Встановіть в якості контента елемента з класом testParagraph 
 * наступний параграф - This is a paragraph
 */

const testParagraph = document.querySelector('#testParagraph');
testParagraph.innerText = 'This is a paragraph';
console.log(testParagraph);

// ===================================================================
/**
 * Отримати елементи <li> вкладені в елемент із класом main-header
 *  і вивести їх у консоль. 
 * Кожному з елементів присвоїти новий клас nav-item.
 */

const mainHeaderLi = document.querySelectorAll('.main-header li');
console.log(mainHeaderLi);

for (let elem of mainHeaderLi) {
elem.className += ' nav-item';
console.log(elem);
};

// ================================================================

/**
 * Знайти всі елементи із класом section-title. 
 * Видалити цей клас у цих елементів.
 */
const sectionTitle = document.querySelectorAll('.section-title');

for (let elem of sectionTitle) {
    elem.classList.remove('section-title');
};
console.log(sectionTitle);


